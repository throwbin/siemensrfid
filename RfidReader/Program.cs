﻿using System;
using System.Diagnostics;
using System.Text;
using System.Timers;
using ReaderLibrary;

namespace RfidReader
{
    class Program
    {
        private static ReaderDLL reader = new ReaderDLL();
        private static int ReaderHandle = -1;
        private static bool isCardDetected;
        private const int POLL_INTERVAL = 500;

        static Timer pollForCardTimer = new Timer();

        static void Main(string[] args)
        {
            pollForCardTimer.Elapsed += pollForCardTimerCallback;
            AppDomain.CurrentDomain.DomainUnload += CleanupBeforeExit;

            StartScan();
          

            Console.ReadKey();
        }

        private static void CleanupBeforeExit(object sender, EventArgs e)
        {
            StopScan();
        }

        public static void StartScan()
        {
            if (ReaderHandle != -1)
            {
                // handle unexpected ReaderHandle0000
                if (ReaderHandle != 0)
                {
                    // if handle is complete invalid, we can get an exception
                    try
                    {
                        reader.brp_close_session(ReaderHandle);
                        Console.WriteLine("ERROR: Reader was allready opened!");
                    }
                    catch (Exception)
                    {
                        Console.WriteLine("ERROR: Unkown ReaderHandle " + ReaderHandle);
                        throw;
                    }
                    ReaderHandle = -1;
                }
                else
                {
                    Console.WriteLine("ERROR: Reader is allready opened!");
                }
                return;
            }

            // - open USB port ---------------------------------------------------------------
            uint productID = 0;
            int return_val = reader.brp_open_usb_session(ref ReaderHandle, productID);
            if (bCheckForError(return_val, "brp_open_usb_session"))
            {
                return;
            }
            Console.WriteLine("Card reader found");

            // - check device status ------------------------------------------------------------
            uint boot_status = 0;
            int Status = 0;

            return_val = reader.syscmd_get_boot_status(ReaderHandle, ref boot_status, ref Status);

            if (bCheckForError(return_val, "syscmd_get_boot_status"))
            {
                try
                {
                    return_val = reader.syscmd_reset(ReaderHandle, ref Status);
                    reader.brp_close_session(ReaderHandle);
                    Console.WriteLine("ERROR: Reader is not responding! syscmd_reset() was called");
                }
                catch (Exception)
                {
                    Console.WriteLine("ERROR: Reader is not responding!");
                }
                ReaderHandle = -1;
                return;
            }
            Console.WriteLine("BootStatus: " + boot_status + " Status: " + Status);

            //- get FW Version ----------------------------------------------------------------
            byte[] SnrByteArray = null;
            reader.syscmd_get_info(ReaderHandle, ref SnrByteArray, ref Status);

            Console.WriteLine("Firmware version: " + System.Text.Encoding.Default.GetString(SnrByteArray));

            // - Scan for cards ---------------------------------------------------------------
            Console.WriteLine("");
            Console.WriteLine("--------------- Reader is scanning for cards---");

            isCardDetected = false;
            pollForCardTimer.Interval = POLL_INTERVAL;
            pollForCardTimer.Start();
        }

        private static bool bCheckForError(Int32 return_val, string functionName)
        {
            if (return_val != ReaderDLL.BRP_OK)
            {
                Console.WriteLine("ERROR in " + functionName + "! Return value:" + return_val.ToString());
                return true;
            }
            return false;
        }

        private static void StopScan()
        {
            if (ReaderHandle != -1)
            {
                reader.brp_close_session(ReaderHandle);
                Console.WriteLine("--------------- Reader stopped ---------------");
                Console.WriteLine("");
                ReaderHandle = -1;
            }
            pollForCardTimer.Stop();
        }

        private static void pollForCardTimerCallback(Object myObject, EventArgs myEventArgs)
        {
            byte CardType = 0;
            int return_val = 0;
            int Status = 0;

            if (!isCardDetected)
            {
                // check if card is in field
                ushort CardTypeMask = 0xFFFF;
                bool Reselect = true;
                bool AllowConfig = false;
                return_val = reader.vhl_select(ReaderHandle, CardTypeMask, Reselect, AllowConfig, ref CardType, ref Status);

                // If no card is in field we are finished
                if (Status == ReaderDLL.VHL_ERR_NOTAG || Status == ReaderDLL.VHL_ERR_HF || Status == ReaderDLL.VHL_ERR_CONFCARD_READ)
                {
                    return;
                }

                // check for error
                if (bCheckForError(return_val, "vhl_select"))
                {
                    return;
                }

                // - We got a card. 
                Console.WriteLine("Card detected. Card type: {0:X}", CardType);

                // - Now we get the serial number ------------------------------------------------------------
                byte[] SnrByteArray = null;
                byte Length = 0;

                return_val = reader.vhl_get_snr(ReaderHandle, ref SnrByteArray, ref Length, ref Status);
                if (bCheckForError(return_val, "vhl_get_snr") || Length <= 0)
                {
                    return;
                }

                StringBuilder serialNumberText = new StringBuilder("Serial Number: 0x");
                for (int i = 0; SnrByteArray != null && i < SnrByteArray.Length; i++)
                {
                    serialNumberText.Append($"{SnrByteArray[SnrByteArray.Length - 1 - i]:X2}");
                }
                Console.WriteLine(serialNumberText.ToString());

                // Set LED
                return_val = reader.syscmd_set_port(ReaderHandle, ReaderDLL.LED_GREEN, ref Status);
                isCardDetected = true;

                // ------------------------------------------------------
                // Here you could add your code to handle "card in field"
                //-------------------------------------------------------

            }
            else
            {
                // - check if card has left the reader field ----------------------------------------------
                return_val = reader.vhl_is_selected(ReaderHandle, ref Status);

                // if card is still in field, we are finished 
                if (return_val == ReaderDLL.BRP_OK && Status == ReaderDLL.BRP_OK)
                {
                    return;
                }

                // check for error
                if (Status != ReaderDLL.VHL_ERR_CARD_NOT_SELECTED && bCheckForError(return_val, "vhl_is_selected"))
                {
                    return;
                }

                Console.WriteLine("Card left field");

                // - reset LED -------------------------------------------------------------------------
                return_val = reader.syscmd_set_port(ReaderHandle, ReaderDLL.LED_OFF, ref Status);
                if (bCheckForError(return_val, "syscmd_set_port"))
                {
                    return;
                }
                // ------------------------------------------------------
                // Here you could add your code to handle "card leave field"
                //-------------------------------------------------------
                isCardDetected = false;
            }
        }
    }
}
