﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace ReaderLibrary
{

    public class ReaderDLL
    {


        #region Constants and Definitions

        /// <summary>
        /// DLL function return codes
        /// </summary>
        public const Int32 BRP_OK = 0x00;                      // no error occured
        public const Int32 BRP_ERR_STATUS = 0x01;              // the reader returned a statuscode != 0
        public const Int32 BRP_ERR_BUSY = 0x02;                // the reader is working up a command
        public const Int32 BRP_ERR_IDLE = 0x03;                // the reader is waiting for a command
        public const Int32 BRP_ERR_TIMEOUT = 0x04;             // the response/alivemsg timed out
        public const Int32 BRP_ERR_CORRUPTED_FRAME = 0x05;     // the reader sent a corrupted frame
        public const Int32 BRP_ERR_UNEXPECTED_FRAME = 0x06;    // the reader sent a frame during idle state
        public const Int32 BRP_ERR_GENERAL_IO = 0x07;          // the underlying serial port generated an error
        public const Int32 BRP_ERR_BUFFER_OVERFLOW = 0x08;     // the reader sends more than max_resp_len bytes or
                                                               // the application tries to send more than send_bufsize
                                                               // bytes (see brp_setup_session)
        public const Int32 BRP_ERR_NO_MORE_HANDLES = 0x09;     // there are no free brp handles
        public const Int32 BRP_ERR_INSUFFICIENT_MEM = 0x0A;    // there is not enough memory to create a new BRP Handle
        public const Int32 BRP_ERR_WRONG_HANDLE = 0x0B;        // the handle specified is not present
        public const Int32 BRP_ERR_WRONG_PARAMETERS = 0x0C;    // the parameters of a command were not right
        public const Int32 BRP_ERR_RS485_NO_RESPONSE = 0x0D;   // the rs485 device is not connected or has a invalid address
        public const Int32 BRP_ERR_CRYPTO = 0x0E;              // invalid crypto key / encrypted command/response is invalid

        /// <summary>
        /// defines stati of reader
        /// </summary> 
        public const Int32 VHL_ERR_NOTAG = 0x0101;
        public const Int32 VHL_ERR_CARD_NOT_SELECTED = 0x0102;
        public const Int32 VHL_ERR_HF = 0x0103;
        public const Int32 VHL_ERR_CONFIG_ERR = 0x0104;
        public const Int32 VHL_ERR_AUTH = 0x0105;
        public const Int32 VHL_ERR_READ = 0x0106;
        public const Int32 VHL_ERR_WRITE = 0x0107;
        public const Int32 VHL_ERR_CONFCARD_READ = 0x0108;
        public const Int32 VHL_ERR_INVALID_CARD_FAMILY = 0x0109;
        public const Int32 VHL_ERR_NOT_SUPPORTED = 0x010A;

        /// <summary>
        /// defines LED colors
        /// </summary>
        public const UInt16 LED_OFF = 0x000;
        public const UInt16 LED_GREEN = 0x001;
        public const UInt16 LED_RED = 0x002;
        public const UInt16 LED_ORANGE = 0x003;

        #endregion

        #region members
        bool is64BitProcess = true;
        #endregion

        /// <summary>
        /// Constructor
        /// </summary>
        public ReaderDLL()
        {
            is64BitProcess = (IntPtr.Size == 8);
        }

        /// <summary>
        /// Open USB session and return handle if Reader is connected
        /// </summary>
        /// <param name="Handle">Output parameter. Returns handle of reader</param>
        /// <param name="productID">should always be zero</param>
        /// <returns>Returns 0 on success</returns>
        public Int32 brp_open_usb_session(ref Int32 Handle, UInt32 productID)
        {
            if (is64BitProcess)
            {
                return ReaderDllx64.brp_open_usb_session(ref Handle, productID);
            }
            else
            {
                return ReaderDllx86.brp_open_usb_session(ref Handle, productID);
            }
        }

        /// <summary>
        /// Close USB session and free resources
        /// </summary>
        /// <param name="Handle">Handle of reader</param>
        /// <returns></returns>
        public Int32 brp_close_session(Int32 Handle)
        {
            if (is64BitProcess)
            {
                return ReaderDllx64.brp_close_session(Handle);
            }
            else
            {
                return ReaderDllx86.brp_close_session(Handle);
            }
        }

        /// <summary>
        /// Reboots the reader.
        /// </summary>
        /// <param name="Handle">Handle of reader</param>
        /// <param name="Status">Output parameter: Returns status code of reader</param>
        /// <returns>Returns 0 on success</returns>
        public Int32 syscmd_reset(Int32 Handle, ref Int32 Status)
        {
            if (is64BitProcess)
            {
                return ReaderDllx64.syscmd_reset(Handle, ref Status);
            }
            else
            {
                return ReaderDllx86.syscmd_reset(Handle, ref Status);
            }
        }

        /// <summary>
        /// Retrieve the firmware version of the reader.
        /// </summary>
        /// <param name="Handle">Handle of reader</param>
        /// <param name="fws">Firmware version string</param>
        /// <param name="Status">Output parameter: Returns status code of reader</param>
        /// <returns>Returns 0 on success</returns>
        public Int32 syscmd_get_info(Int32 Handle, ref byte[] Fws, ref Int32 Status)
        {
            byte maxLength = 80;
            Int32 return_val = BRP_ERR_GENERAL_IO;

            IntPtr FwsBuffer = Marshal.AllocHGlobal(Marshal.SizeOf(maxLength) * maxLength);
            try
            {
                if (is64BitProcess)
                {
                    return_val = ReaderDllx64.syscmd_get_info(Handle, FwsBuffer, ref Status);
                }
                else
                {
                    return_val = ReaderDllx86.syscmd_get_info(Handle, FwsBuffer, ref Status);
                }

                Fws = new byte[maxLength];
                Marshal.Copy(FwsBuffer, Fws, 0, maxLength); //SnrByteArray.Length);

            }
            finally
            {
                Marshal.FreeHGlobal(FwsBuffer);
            }

            return return_val;
        }

        /// <summary>
        /// Check status of reader
        /// </summary>
        /// <param name="Handle">Handle of reader</param>
        /// <param name="boot_status">Output parameter: Returns bootstatus word of the reader</param>
        /// <param name="Status">Output parameter: Returns status code of reader</param>
        /// <returns>Returns 0 on success</returns>
        public Int32 syscmd_get_boot_status(Int32 Handle, ref UInt32 boot_status, ref Int32 Status)
        {
            if (is64BitProcess)
            {
                return ReaderDllx64.syscmd_get_boot_status(Handle, ref boot_status, ref Status);
            }
            else
            {
                return ReaderDllx86.syscmd_get_boot_status(Handle, ref boot_status, ref Status);
            }
        }

        /// <summary>
        /// Select card in field, or select the next card in field.
        /// </summary>
        /// <param name="Handle">Handle of reader</param>
        /// <param name="cardTypeMask">Mask for selecting only specific cards. 0xFFFF = all supported cards</param>
        /// <param name="reselect">If you want to reselect cards without moving them out of the antenna's field physically, you have to set the Reselect flag to TRUE</param>
        /// <param name="allowConfig">True = accept config cards</param>
        /// <param name="CardType">Output parameter: Returns type of card</param>
        /// <param name="Status">Output parameter: Returns status code of reader</param>
        /// <returns></returns>
        public Int32 vhl_select(Int32 Handle, UInt16 cardTypeMask, bool reselect, bool allowConfig, ref byte CardType, ref Int32 Status)
        {
            if (is64BitProcess)
            {
                return ReaderDllx64.vhl_select(Handle, cardTypeMask, reselect, allowConfig, ref CardType, ref Status);
            }
            else
            {
                return ReaderDllx86.vhl_select(Handle, cardTypeMask, reselect, allowConfig, ref CardType, ref Status);
            }
        }

        /// <summary>
        /// Get serial number of selected card
        /// </summary>
        /// <param name="Handle">Handle of reader</param>
        /// <param name="Snr">Byte array containig serial number</param>
        /// <param name="Length">Length of serial number</param>
        /// <param name="Status">Output parameter: Returns status code of reade</param>
        /// <returns></returns>
        public Int32 vhl_get_snr(Int32 Handle, ref byte[] Snr, ref byte Length, ref Int32 Status)
        {

            byte maxLength = 40;
            Int32 return_val = BRP_ERR_GENERAL_IO;

            IntPtr SnrBuffer = Marshal.AllocHGlobal(Marshal.SizeOf(maxLength) * maxLength);

            try
            {
                if (is64BitProcess)
                {
                    return_val = ReaderDllx64.vhl_get_snr(Handle, SnrBuffer, ref Length, ref Status);
                }
                else
                {
                    return_val = ReaderDllx86.vhl_get_snr(Handle, SnrBuffer, ref Length, ref Status);
                }

                Snr = new byte[Length];
                Marshal.Copy(SnrBuffer, Snr, 0, Length); //SnrByteArray.Length);
            }
            finally
            {
                Marshal.FreeHGlobal(SnrBuffer);
            }

            return return_val;
        }

        /// <summary>
        /// Set LED 
        /// </summary>
        /// <param name="Handle">Handle of reader</param>
        /// <param name="port_mask">Color of LED</param>
        /// <param name="Status">Output parameter: Returns status code of reade</param>
        /// <returns></returns>
        public Int32 syscmd_set_port(Int32 Handle, UInt16 port_mask, ref Int32 Status)
        {
            if (is64BitProcess)
            {
                return ReaderDllx64.syscmd_set_port(Handle, port_mask, ref Status);
            }
            else
            {
                return ReaderDllx86.syscmd_set_port(Handle, port_mask, ref Status);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Handle">Handle of reader</param>
        /// <param name="Status">Output parameter: Returns status code of reade</param>
        /// <returns></returns>
        public Int32 vhl_is_selected(Int32 Handle, ref Int32 Status)
        {
            if (is64BitProcess)
            {
                return ReaderDllx64.vhl_is_selected(Handle, ref Status);
            }
            else
            {
                return ReaderDllx86.vhl_is_selected(Handle, ref Status);
            }
        }

    }
    /// <summary>
    /// Managed class for accessing the functions of the 64 bit RFID Reader DLL 
    /// </summary>
    public class ReaderDllx64
    {
        // DLL location
        const string _BrpDriver_x64 = "ReaderDLL\\BrpDriver_x64.dll";

        /// <summary>
        /// Open USB session and return handle if Reader is connected
        /// </summary>
        /// <param name="Handle">Output parameter. Returns handle of reader</param>
        /// <param name="productID">should always be zero</param>
        /// <returns>Returns 0 on success</returns>
        [DllImport(_BrpDriver_x64, CallingConvention = CallingConvention.Cdecl)]
        public static extern Int32 brp_open_usb_session(ref Int32 Handle, UInt32 productID);

        /// <summary>
        /// Close USB session and free resources
        /// </summary>
        /// <param name="Handle">Handle of reader</param>
        /// <returns></returns>
        [DllImport(_BrpDriver_x64, CallingConvention = CallingConvention.Cdecl)]
        public static extern Int32 brp_close_session(Int32 Handle);

        /// <summary>
        /// Reboots the reader.
        /// </summary>
        /// <param name="Handle">Handle of reader</param>
        /// <param name="Status">Output parameter: Returns status code of reader</param>
        /// <returns>Returns 0 on success</returns>
        [DllImport(_BrpDriver_x64, CallingConvention = CallingConvention.Cdecl)]
        public static extern Int32 syscmd_reset(Int32 Handle, ref Int32 Status);

        /// <summary>
        /// Retrieve the firmware version of the reader.
        /// </summary>
        /// <param name="Handle">Handle of reader</param>
        /// <param name="fws">Firmware version string</param>
        /// <param name="Status">Output parameter: Returns status code of reader</param>
        /// <returns>Returns 0 on success</returns>
        [DllImport(_BrpDriver_x64, CallingConvention = CallingConvention.Cdecl)]
        public static extern Int32 syscmd_get_info(Int32 Handle, IntPtr fws, ref Int32 Status);

        /// <summary>
        /// Check status of reader
        /// </summary>
        /// <param name="Handle">Handle of reader</param>
        /// <param name="boot_status">Output parameter: Returns bootstatus word of the reader</param>
        /// <param name="Status">Output parameter: Returns status code of reader</param>
        /// <returns>Returns 0 on success</returns>
        [DllImport(_BrpDriver_x64, CallingConvention = CallingConvention.Cdecl)]
        public static extern Int32 syscmd_get_boot_status(Int32 Handle, ref UInt32 boot_status, ref Int32 Status);

        /// <summary>
        /// Select card in field, or select the next card in field.
        /// </summary>
        /// <param name="Handle">Handle of reader</param>
        /// <param name="cardTypeMask">Mask for selecting only specific cards. 0xFFFF = all supported cards</param>
        /// <param name="reselect">If you want to reselect cards without moving them out of the antenna's field physically, you have to set the Reselect flag to TRUE</param>
        /// <param name="allowConfig">True = accept config cards</param>
        /// <param name="CardType">Output parameter: Returns type of card</param>
        /// <param name="Status">Output parameter: Returns status code of reader</param>
        /// <returns></returns>
        [DllImport(_BrpDriver_x64, CallingConvention = CallingConvention.Cdecl)]
        public static extern Int32 vhl_select(Int32 Handle, UInt16 cardTypeMask, bool reselect, bool allowConfig, ref byte CardType, ref Int32 Status);

        /// <summary>
        /// Get serial number of selected card
        /// </summary>
        /// <param name="Handle">Handle of reader</param>
        /// <param name="Snr">Byte array containig serial number</param>
        /// <param name="Length">Length of serial number</param>
        /// <param name="Status">Output parameter: Returns status code of reade</param>
        /// <returns></returns>
        [DllImport(_BrpDriver_x64, CallingConvention = CallingConvention.Cdecl)]
        public static extern Int32 vhl_get_snr(Int32 Handle, IntPtr Snr, ref byte Length, ref Int32 Status);

        /// <summary>
        /// Set LED 
        /// </summary>
        /// <param name="Handle">Handle of reader</param>
        /// <param name="port_mask">Color of LED</param>
        /// <param name="Status">Output parameter: Returns status code of reade</param>
        /// <returns></returns>
        [DllImport(_BrpDriver_x64, CallingConvention = CallingConvention.Cdecl)]
        public static extern Int32 syscmd_set_port(Int32 Handle, UInt16 port_mask, ref Int32 Status);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Handle">Handle of reader</param>
        /// <param name="Status">Output parameter: Returns status code of reade</param>
        /// <returns></returns>
        [DllImport(_BrpDriver_x64, CallingConvention = CallingConvention.Cdecl)]
        public static extern Int32 vhl_is_selected(Int32 Handle, ref Int32 Status);
    }

    /// <summary>
    /// Managed class for accessing the functions of the 64 bit RFID Reader DLL 
    /// </summary>
    public class ReaderDllx86
    {
        const string _BrpDriver_x86 = "ReaderDLL\\BrpDriver_x86.dll";

        /// <summary>
        /// Open USB session and return handle if Reader is connected
        /// </summary>
        /// <param name="Handle">Output parameter. Returns handle of reader</param>
        /// <param name="productID">should always be zero</param>
        /// <returns>Returns 0 on success</returns>
        [DllImport(_BrpDriver_x86, CallingConvention = CallingConvention.Cdecl)]
        public static extern Int32 brp_open_usb_session(ref Int32 Handle, UInt32 productID);

        /// <summary>
        /// Close USB session and free resources
        /// </summary>
        /// <param name="Handle">Handle of reader</param>
        /// <returns></returns>
        [DllImport(_BrpDriver_x86, CallingConvention = CallingConvention.Cdecl)]
        public static extern Int32 brp_close_session(Int32 Handle);

        /// <summary>
        /// Check status of reader
        /// </summary>
        /// <param name="Handle">Handle of reader</param>
        /// <param name="boot_status">Output parameter: Returns bootstatus word of the reader</param>
        /// <param name="Status">Output parameter: Returns status code of reader</param>
        /// <returns>Returns 0 on success</returns>
        [DllImport(_BrpDriver_x86, CallingConvention = CallingConvention.Cdecl)]
        public static extern Int32 syscmd_get_boot_status(Int32 Handle, ref UInt32 boot_status, ref Int32 Status);

        /// <summary>
        /// Reboots the reader.
        /// </summary>
        /// <param name="Handle">Handle of reader</param>
        /// <param name="Status">Output parameter: Returns status code of reader</param>
        /// <returns>Returns 0 on success</returns>
        [DllImport(_BrpDriver_x86, CallingConvention = CallingConvention.Cdecl)]
        public static extern Int32 syscmd_reset(Int32 Handle, ref Int32 Status);

        /// <summary>
        /// Retrieve the firmware version of the reader.
        /// </summary>
        /// <param name="Handle">Handle of reader</param>
        /// <param name="fws">Firmware version string</param>
        /// <param name="Status">Output parameter: Returns status code of reader</param>
        /// <returns>Returns 0 on success</returns>
        [DllImport(_BrpDriver_x86, CallingConvention = CallingConvention.Cdecl)]
        public static extern Int32 syscmd_get_info(Int32 Handle, IntPtr fws, ref Int32 Status);

        /// <summary>
        /// Select card in field, or select the next card in field.
        /// </summary>
        /// <param name="Handle">Handle of reader</param>
        /// <param name="cardTypeMask">Mask for selecting only specific cards. 0xFFFF = all supported cards</param>
        /// <param name="reselect">If you want to reselect cards without moving them out of the antenna's field physically, you have to set the Reselect flag to TRUE</param>
        /// <param name="allowConfig">True = accept config cards</param>
        /// <param name="CardType">Output parameter: Returns type of card</param>
        /// <param name="Status">Output parameter: Returns status code of reader</param>
        /// <returns></returns>
        [DllImport(_BrpDriver_x86, CallingConvention = CallingConvention.Cdecl)]
        public static extern Int32 vhl_select(Int32 Handle, UInt16 cardTypeMask, bool reselect, bool allowConfig, ref byte CardType, ref Int32 Status);

        /// <summary>
        /// Get serial number of selected card
        /// </summary>
        /// <param name="Handle">Handle of reader</param>
        /// <param name="Snr">Byte array containig serial number</param>
        /// <param name="Length">Length of serial number</param>
        /// <param name="Status">Output parameter: Returns status code of reade</param>
        /// <returns></returns>
        [DllImport(_BrpDriver_x86, CallingConvention = CallingConvention.Cdecl)]
        public static extern Int32 vhl_get_snr(Int32 Handle, IntPtr Snr, ref byte Length, ref Int32 Status);

        /// <summary>
        /// Set LED 
        /// </summary>
        /// <param name="Handle">Handle of reader</param>
        /// <param name="port_mask">Color of LED</param>
        /// <param name="Status">Output parameter: Returns status code of reade</param>
        /// <returns></returns>
        [DllImport(_BrpDriver_x86, CallingConvention = CallingConvention.Cdecl)]
        public static extern Int32 syscmd_set_port(Int32 Handle, UInt16 port_mask, ref Int32 Status);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Handle">Handle of reader</param>
        /// <param name="Status">Output parameter: Returns status code of reade</param>
        /// <returns></returns>
        [DllImport(_BrpDriver_x86, CallingConvention = CallingConvention.Cdecl)]
        public static extern Int32 vhl_is_selected(Int32 Handle, ref Int32 Status);
    }  
}
